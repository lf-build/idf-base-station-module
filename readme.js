const fs = require('fs');
const package = require('./package.json');
const comments = require('parse-comments'); 
const path = require('path');
const workflowRoot = path.join(__dirname, 'workflow');
let toc = {};
const template = `### {name}
> {description}

\`\`\`
POST http://<api-host>:<api-port>/<client-id>/{path}
headers: {
    "Authorization": <token>,
}
{schema}
\`\`\`
---
`

const stages = fs.readdirSync(workflowRoot)
                   .map(stage => [stage, path.join(workflowRoot, stage)])
                   .filter(([stage, stageRoot]) => fs.statSync(stageRoot).isDirectory())
                   .reduce((a, [stage, stageRoot]) => {

                       a[stage] = fs.readdirSync(stageRoot)
                                .map(fact => [fact, path.join(stageRoot, fact)])
                                .filter(([fact, factRoot]) =>  fs.statSync(factRoot).isDirectory())
                                .reduce((a, [fact, factRoot]) => {
                                  a[fact]= fs.readdirSync(factRoot)
                                             .filter(action => action.endsWith('.js'))
                                             .map(action => [action.replace('.js', ''), path.join(factRoot, action)])
                                             .filter(([action, actionRoot]) => !action.startsWith('.') && !action.endsWith('.specs') && fs.statSync(actionRoot).isFile())
                                             .reduce((a, [action, actionRoot]) => {
                                               comments(fs.readFileSync(actionRoot, 'utf8')).forEach((parsedMessage) => {
                                                const { description, category, section, name } = parsedMessage;
                                                const [schema] = require(actionRoot);
                                                if (section && category && name) {
                                                  if(!toc[section]) {
                                                    toc[section] = {};
                                                  }
                                                  if(!toc[section][category]) {
                                                    toc[section][category] = [];
                                                  }
                                                  toc[section][category].push({
                                                    path: `${stage}/${fact}/${action}`,
                                                    name,
                                                    body: template.replace(/{name}/g, name)
                                                                      .replace(/{description}/g, description)
                                                                      .replace(/{path}/g, `${stage}/${fact}/${action}`)
                                                                      .replace(/{schema}/g, schema ? JSON.stringify(schema, null, 4) : '')
                                                  });
                                                }
                                               });
                                               return a;
                                             }, {});
                                  return a;
                                }, {});
                     return a;
                   }, {});

module.exports = toc;