const schema = {
    searchText: 'string',
    searchFields: 'array',
    page:'number:min=1,max=100,required',
    sortBy: 'string',
    sortDirection: {
        '@type': 'string',
        regex: /a|d/g
    },
    raw: 'any'
};

function * get({
    $api,
    $set,
    value: {
        searchText,
        searchFields,
        sortBy,
        sortDirection,
        page,
        raw,
    },
    $debug,
    $configuration
}) {
    const { 'application-filters': applicationFilters } = yield $configuration('endpoints');
    const filter = {
        "searchFilterView": {},
        "sorts": {
            [sortBy
                    ? sortBy
                    : "_id"]: sortDirection === 'a'
                ? 1
                : -1
        }
    };

    if (searchText) {
        const searchFilters = [];
        searchFields.forEach((field) => {
            searchFilters
                .push({
                    [field]: {
                        "$regex": searchText,
                        "$options": "i"
                    }
                });
        });
        filter.searchFilterView = {
            $or: searchFilters,
        }
    }

    if (raw) {
        filter.searchFilterView = Object.assign(filter.searchFilterView, raw);
    }

    const response = yield $api.post(`${applicationFilters}/search-applications-free?page=${page}`, filter);

    return {
        items: response.body,
        currentPage: response
            .headers
            .get('X-CURRENT-PAGE'),
        pageSize: response
            .headers
            .get('X-PAGE-SIZE'),
        totalCount: response
            .headers
            .get('X-TOTAL-COUNT'),
        totalPages: response
            .headers
            .get('X-TOTAL-PAGES')
    };
};

module.exports = [schema, get];