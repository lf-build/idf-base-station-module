const schema={
    partnerId:'string:min=2,max=100,required',
};

function* get({
    $get,
    $set,
    value:{partnerId},
    $debug,
    $configuration,
    $api,
}){
    const { metricService } = yield $configuration('endpoints');

    let last30DaysApplications = 0,
        last30DaysApproved = 0,
        last30DaysFunded = 0,
        last30DaysCommission = 0,
        lifeTimeApplications = 0,
        lifeTimeApproved = 0,
        lifeTimeFunded = 0,
        lifeTimeCommission = 0;

    try{
        //Last 30 days commission
        const response = yield $api.get(`${metricService}/application/partner/${partnerId}/30`);
        if(response.body.length > 0){
            response.body.forEach(function(obj) {
                    last30DaysCommission = last30DaysCommission + obj.commissionAmount;
            }, this);
        }
    }
    catch(e){
        $debug(e);
    }

    try{
        //Lifetime commission
        const response = yield $api.get(`${metricService}/application/partner/${partnerId}`);
        if(response.body.length > 0){
            response.body.forEach(function(obj) {
                    lifeTimeCommission = lifeTimeCommission + obj.commissionAmount;
            }, this);
        }
    }
    catch(e){
        $debug(e);
    }

    try{
        //Last 30 days application counts
        const response = yield $api.get(`${metricService}/application/statuscountbypartner/${partnerId}/30`);
        if(response && response.body){
            if(response.body.totalCount){
                last30DaysApplications = response.body.totalCount;
            }
            if(response.body.metricData && response.body.metricData.length > 0){
                const approved = response.body.metricData.filter(function(data){
                        return data.name === '400.20';
                });
                if(approved && approved.length > 0){
                    last30DaysApproved = approved[0].count;
                }
                const funded = response.body.metricData.filter(function(data){
                        return data.name === '400.30';
                });
                if(funded && funded.length > 0){
                    last30DaysFunded = funded[0].count;
                }
            }
        }
    }
    catch(e){
        $debug(e);
    }

    try{
        //Lifetime application counts
        const response = yield $api.get(`${metricService}/application/statuscountbypartner/${partnerId}`);
        if(response && response.body){
            if(response.body.totalCount){
                lifeTimeApplications = response.body.totalCount;
            }
            if(response.body.metricData && response.body.metricData.length > 0){
               const approved = response.body.metricData.filter(function(data){
                        return data.name === '400.20';
                });
                if(approved && approved.length > 0){
                    lifeTimeApproved = approved[0].count;
                }
                const funded = response.body.metricData.filter(function(data){
                        return data.name === '400.30';
                });
                if(funded && funded.length > 0){
                    lifeTimeFunded = funded[0].count;
                }
            }
        }
    }
    catch(e){
        $debug(e);
    }

    return {
        "last30Days":{
            "applications":last30DaysApplications,
            "approved":last30DaysApproved,
            "funded":last30DaysFunded,
            "commission":last30DaysCommission
        },
        "lifeTime":{
            "applications":lifeTimeApplications,
            "approved":lifeTimeApproved,
            "funded":lifeTimeFunded,
            "commission":lifeTimeCommission
        }
    };
};

module.exports=[schema,get];