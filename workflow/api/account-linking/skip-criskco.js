const schema={
    applicationNumber:'string:min=2,max=100,required',
};

function* set({
    $get,
    $set,
    $api,
    value:{ applicationNumber },
    $debug,
    $configuration
}){
     try{
         const { 'application-processor': application } = yield $configuration('endpoints');
         const response = yield $api.post(`${application}/application/${applicationNumber}/skip/financialaccountlinking`);
         return response.body ;
     } catch(e) {
       $debug(e);
       throw {
         message: 'Something went wrong',
         code: 400,
     }
  }
};

module.exports=[schema,set];