const schema={
    PublicToken : "string",
    BankSupportedProductType : "string",
    AccountId : "string",
    EntityId : "string",
    EntityType : "string",
};

/**
 * 
 * Set Plaid Bank
 * 
 * @category Bank Linking
 * @section API
 * @name set
 * 
 * @api public
 * 
 */

function* set({
    $get,
    $set,
    $api,
    value,
    $debug,
    $configuration
}){

   try{
         const { 'webhook-notifier': webhook, configuration } = yield $configuration('endpoints');
         const tokenResponse =  yield $api.get(`${configuration}/webhook-notifier`);
         const response = yield $api.post(`${webhook}/plaid?token=${tokenResponse.body.WebhookTenantTokenId}`, value);
         return response.body ;
     } catch(e) {
       $debug(e);
       throw {
         message: 'Something went wrong',
         code: 400,
     }
  }
};

module.exports=[schema,set];