const schema={
    applicationNumber:'string:min=2,max=100,required',
    businessId:'string:min=2,max=100,required',
};

function* set({
    $get,
    $set,
    $api,
    value:{ applicationNumber,businessId },
    $debug,
    $configuration
}){
     try{
         const payload = {
             businessId,
             EntityId : applicationNumber,
             EntityType : "application"
         };
         const { 'webhook-notifier': webhook, configuration } = yield $configuration('endpoints');
         const tokenResponse =  yield $api.get(`${configuration}/webhook-notifier`);
         const response = yield $api.post(`${webhook}/criskco?token=${tokenResponse.body.WebhookTenantTokenId}`, payload);
         return response.body;
     } catch(e) {
       $debug(e);
       throw {
         message: 'Something went wrong',
         code: 400,
     }
  }
};

module.exports=[schema,set];