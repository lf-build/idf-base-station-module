const schema={
    applicationNumber:'string:min=2,max=100,required',
    token:'string:required'
};

function* set({
    $get,
    $set,
    $api,
    value:{ applicationNumber,token },
    $debug,
    $configuration,
    $stages: {
    'api': {
      'application': {
        'filter': applicationFilter
      }
    }
  },
}){
   try{
    const { 'token-manager':tokenManager, partners: brokerEndpoint , identity: identityEndpoint,'asset-service': assetEndpoint, configuration } = yield $configuration('endpoints');
    let finalResponse = {};
    // Get application data
    const response = yield applicationFilter.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});
  
    if(response.body && response.body.length > 0){
     const PartnerUserId=response.body[0].PartnerUserId;
     const PartnerId=response.body[0].PartnerId;              
         
     // Get partner data
     const partner = yield $api.get(`${identityEndpoint}/user/${PartnerUserId}`);
      
     // fetch broker image
      let brokerImage = null;
      try{
        const imgResponse = yield $api.get(`${assetEndpoint}/user/${partner.body.username}/profile.png`);
        brokerImage = imgResponse.body.url;
      }
      catch(e){
        $debug(e);
      }
     // Fetch broker's personal details
     const partnerPersonalDetail = yield $api.get(`${brokerEndpoint}/${PartnerId}`);

     const payload={
          Token:token,
          EntityType:"application",
          EntityId : applicationNumber  
     };

     const IsTokenValid=yield $api.post(`${tokenManager}/validate`,payload)

     let broker={};
     broker.userId=partner.body.id;
     broker.name=partnerPersonalDetail.body.principalOwner.firstName+' '+partnerPersonalDetail.body.principalOwner.lastName;
     broker.email=partner.body.email,
     broker.phone=partnerPersonalDetail.body.contacts[0].phoneNumber;
     broker.image=brokerImage;
     finalResponse.broker=broker;
     finalResponse.accounting={
         status:true,
     };
     finalResponse.banking={
         status:true,
     };
     finalResponse.IsTokenValid=IsTokenValid.body;
     finalResponse.creditReview={
         status:true,
     }
     // Set account & bank linking status based on tags
     if(response.body[0].tags && response.body[0].tags.length > 0){
          response.body[0].tags.forEach((tag) => {
             if(tag==="Pending Financial Account"){
               finalResponse.accounting.status=false;
             }else if(tag==="Pending Bank Linking"){
               finalResponse.banking.status=false;
             }
             else if(tag==="Pending Credit Report Consent"){
               finalResponse.creditReview.status=false;
             }
          });
     }
    }
    return finalResponse;
   }catch(e){
       $debug(e);
       return{
           code : 500,
           message: e,
       }
   }
};

module.exports=[schema,set];