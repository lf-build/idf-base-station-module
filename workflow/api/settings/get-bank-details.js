var schema={
    partnerId:'string:min=2,max=100,required',
};

function* get({
    $get,
    $set,
    $api,
    value:{partnerId},
    $debug,
    $configuration
}){
   try{
    const { partners: brokerEndpoint } = yield $configuration('endpoints');
    const response = yield $api.get(`${brokerEndpoint}/${partnerId}/banks`);
    let finalResponse = {};
    if(response.body.length > 0){
      finalResponse.partnerId = partnerId;
      finalResponse.bankId = response.body[0].id;
      finalResponse.bankName = response.body[0].name;
      finalResponse.bankAccountName = response.body[0].accountHolderName;
      finalResponse.bankAccountNumber = response.body[0].accountNumber;
      finalResponse.aba = response.body[0].routingNumber;
      finalResponse.accountType = response.body[0].accountType === "Savings" ? "1" : "2";
    }
    else{
      finalResponse.partnerId = partnerId;
      finalResponse.bankId = "";
    }
    return finalResponse;
  } catch(e) {
    $debug(e);
    return {partnerId : partnerId};
  }
};

module.exports=[schema,get];