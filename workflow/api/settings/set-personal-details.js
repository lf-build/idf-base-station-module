var schema={
          firstName: 'string:min=2,max=100,required',
          lastName: 'string:min=2,max=100,required',
          dateOfBirth:'string:required',
          street:'string:min=2,max=100,required',
          city:'string::min=2,max=100,required',
          postCode:'string:min=5,max=5,required',
          mobileNumber:'string:min=10,max=10,required',
          partnerId:'string::min=2,max=100,required',
          companyName:'string::min=2,max=100,required',
};

function* set({
    $get,
    $set,
    $api,
    value:{firstName,lastName,dateOfBirth,street,city,postCode,mobileNumber,partnerId,companyName},
    $debug,
    $configuration
}){
    {
        const { partners: brokerEndpoint } = yield $configuration('endpoints');
        let newResponse = {};
        try{
            const response = yield $api.get(`${brokerEndpoint}/${partnerId}`);
            if(response !== null || response !== undefined || response !== ""){
                response.body.contacts[0].phoneNumber = mobileNumber;
                response.body.address.Line1 = street;
                response.body.address.city = city;
                response.body.address.zipCode = postCode;
                response.body.principalOwner.firstName = firstName;
                response.body.principalOwner.lastName = lastName;
                response.body.principalOwner.dateOfBirth = dateOfBirth;
                let finalRequest = {
                    nickName : response.body.nickName,
                    partnerType : response.body.partnerType,
                    address : response.body.address,
                    principalOwner : response.body.principalOwner,
                    contacts : response.body.contacts,
                    bankAccounts: response.body.bankAccounts,
                    notes: '',
                    name:companyName,
                };
                newResponse = yield $api.put(`${brokerEndpoint}/${partnerId}`,finalRequest);
            }    
        } catch(e) {
            $debug(e);
            throw {
                message: 'Something went wrong',
                code: 400,
            }
        }
        return {firstName,lastName,dateOfBirth,street,city,postCode,mobileNumber,partnerId,companyName};
    }
};

module.exports=[schema,set];