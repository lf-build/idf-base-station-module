var FormData = require('form-data');
const schema = {
   userName: 'string:min=2,max=100,required',
   fileList: {
        '@items': {
          file: 'string:min=2,max=100,required',
          dataUrl: 'string:min=2,max=100000000,required',
        },
        max: 1,
        min: 1,
    }
};
function *set({
  $api, 
  $set, 
  value:{
    userName,
    fileList,
  },
  $debug,
  $configuration})  {

   const { identity: identityEndpoint,'asset-service': assetEndpoint} = yield $configuration('endpoints');
  
  try{
        const fileObject = fileList[0];
        let brokerImage = null;
        try{
          const imgResponse = yield $api.get(`${assetEndpoint}/user/${userName}/profile.png`);
          brokerImage = imgResponse.body.url;
        }
        catch(ex){
          $debug(ex);
        }
        const formData = new FormData();
        formData.append('file', new Buffer(fileObject.dataUrl.substring(fileObject.dataUrl.indexOf("base64,")).replace('base64,',''), 'base64'), fileObject.file);
        formData.append('filename',"profile.png")
        if(brokerImage==null){
          const imgResponse = yield $api.post(`${assetEndpoint}/user/${userName}`, formData,null);
        }else{
          const imgResponse = yield $api.put(`${assetEndpoint}/user/${userName}/profile.png`,formData,null);
        }
  }
  catch(e){
    $debug(e);
    throw {      
       message: 'Something went wrong',
       code: 400,
      }
  } 
  return {};
};

module.exports = [schema, set];