var schema={
    partnerId:'string:min=2,max=100,required',
};

function* get({
    $get,
    $set,
    $api,
    value:{partnerId},
    $debug,
    $configuration
}){
 try{
    const { partners: brokerEndpoint } = yield $configuration('endpoints');
    const response = yield $api.get(`${brokerEndpoint}/${partnerId}`);
    finalDOB = `${response.body.principalOwner.dateOfBirth.substring(5, 7)}/${response.body.principalOwner.dateOfBirth.substring(8, 10)}/${response.body.principalOwner.dateOfBirth.substring(0, 4)}`;
    let finalResponse = {
        firstName:response.body.principalOwner.firstName,
        lastName:response.body.principalOwner.lastName,
        dateOfBirth:finalDOB,
        street:response.body.address.line1,
        city:response.body.address.city,
        postCode:response.body.address.zipCode,
        mobileNumber:response.body.contacts[0].phoneNumber,
        partnerId:response.body.partnerId,
        companyName:response.body.name,
    };
    return finalResponse;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];