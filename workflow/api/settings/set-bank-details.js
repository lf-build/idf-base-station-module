var schema = {
    partnerId:'string:min=2,max=100,required',
    bankId:'string',
    //companyName: 'string',
    bankName: 'string:min=2,max=100,required',
    bankAccountName: 'string:min=2,max=100,required',
    bankAccountNumber: 'string:min=2,max=100,required',
    aba: 'string:min=2,max=100,required',
    accountType:'number:required:min=1,max=1,required'
};

function * set({
    $get, 
    $set, 
    $api,
    value:{partnerId,companyName,bankName,bankAccountName,bankAccountNumber,aba,accountType,bankId},
     $debug, 
     $configuration
    }) {
          const { partners: brokerEndpoint } = yield $configuration('endpoints');
          const requestBody={
            accountNumber: bankAccountNumber,
            routingNumber: aba,
            accountType: accountType,
            accountHolderName:bankAccountName,
            Name : bankName,
            isPrimary:false,
            }

          try{
            let response = {};
            if(bankId === "" || bankId === undefined){
              //Insert banks
              response = yield $api.post(`${brokerEndpoint}/${partnerId}/banks`,requestBody);
            } 
            else{
             //Update bank
              response = yield $api.put(`${brokerEndpoint}/${partnerId}/banks/${bankId}`,requestBody);
            }
            let finalResponse = {};
              finalResponse.partnerId = partnerId;
              finalResponse.bankId = response.body.id;
              finalResponse.bankName = response.body.name;
              finalResponse.bankAccountName = response.body.accountHolderName;
              finalResponse.bankAccountNumber = response.body.accountNumber;
              finalResponse.aba = response.body.routingNumber;
              finalResponse.accountType = response.body.accountType === "Savings" ? "1" : "2";
            return finalResponse;
          }
          catch(e) {
            $debug(e);
              throw {
                message: 'Something went wrong',
                code: 400,
            }
        }
    };

module.exports = [schema, set];
