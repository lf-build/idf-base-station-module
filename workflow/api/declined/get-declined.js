const schema={
    applicationNumber:'string:min=2,max=100,required',
};

function* get({
    $get,
    $set,
    $api,
    value:{applicationNumber,sortDirection},
    $debug,
    $configuration,
    $stages: {
    'api': {
      'application': {
        'filter': applicationFilter
      }
    }
  },
}){
    const { lookupservice } = yield $configuration('endpoints');
    
    let masterDeclineReasons = {};
    try{
        const declinereasons =  yield $api.get(`${lookupservice}/declinereasons`);
        tempDeclineReasons = declinereasons.body;
        var key, keys = Object.keys(tempDeclineReasons);
        var n = keys.length;
        while (n--) {
          key = keys[n];
          masterDeclineReasons[key.toLowerCase()] = tempDeclineReasons[key];
        }
    }
    catch(e){
        $debug(e);
    }

    try{
        let finalResponse = {};
        finalResponse.declineReasons = [];
        const response = yield applicationFilter.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});

        if(response.body && response.body.length > 0){
            if(response.body[0].DeclineReason && response.body[0].DeclineReason.length > 0)
            {
                const Reasons = response.body[0].DeclineReason;
                Reasons.forEach(function(reason) {
                    const lowerReason = reason.toLowerCase();
                    const reasonDescription = masterDeclineReasons[lowerReason];
                    if(reasonDescription){
                       const array = reasonDescription.split(':');
                       if(array && array.length === 2){
                           finalResponse.declineReasons.push(
                        {  reason: array[0],
                        subReason: array[1],
                        });
                       }
                    }
                }, this);
            }
            finalResponse.applicationDetails = {};
            finalResponse.applicationDetails.loanAmount = response.body[0].AmountFunded;

            let finalOwnerList = [];
            if(response.body[0].Owners && response.body[0].Owners.length > 0){
                response.body[0].Owners.forEach(function(element) {
                    finalOwnerList.push(`${element.FirstName} ${element.LastName}`);
                }, this);
            }

            finalResponse.applicationDetails.applicants = finalOwnerList;
            finalResponse.applicationDetails.businessName = response.body[0].LegalBusinessName;
            finalResponse.applicationDetails.repaymentAmount = response.body[0].RepaymentAmount;
            finalResponse.applicationDetails.sellRate = response.body[0].SellRate;
            finalResponse.applicationDetails.term = response.body[0].Term;
            finalResponse.applicationDetails.commission = response.body[0].CommissionAmount;
        }
        return finalResponse;
    } 
    catch(e){
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports=[schema,get];