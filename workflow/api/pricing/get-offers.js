var schema={
    applicationNumber:'string:min=2,max=100,required',
};

function* get({
$get,
$set,
$api,
value:{applicationNumber},
$debug,
$configuration,
$stages: {
    'api': {
      'application': {
        'get-offers': getOffers
      }
    }
  },
}){
  try{
       const response = yield getOffers.$execute
       .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});
      const offer={
                 applicationNumber:applicationNumber,
                 divergentGrade:response.offers ? response.offers[0].grade: '',
                 amountFunded:0,
                 amountPayback:0,
                 commission:0,
                 commissionPercentage:0,
                 commisionFormula:"formula1",
                 advanceFormula:"formula2",
                 dailyPayment:0,
                 sellRate:0,
                 term:0,
                 divergentReturn:0,
                 divergentFR:0,
                 afterDefaultAssumption:0,
                 averageLife:0,
                 iir:0,
                 durationIncremental:0,
                 sellIncremental:0,
                 monthlyRevenue: response.monthlyRevenue,
                 averageDailyBalance: response.averageDailyBalance,
                 durationType:response.offers ? response.offers[0].durationType : '',
                 range:{
                        dailyPayment:{
                            min:response.offers ? response.offers[0].minDailyPayment:0,
                            max:response.offers ? response.offers[0].maxDailyPayment:0
                        },
                        sellRate:{
                            min:response.offers ? response.offers[0].minFactor:0,
                            max:response.offers ? response.offers[0].maxFactor:0
                        },
                        term:{
                            min:response.offers ? response.offers[0].minDuration:0,
                            max:response.offers ? response.offers[0].maxDuration:0
                        }
                     }
                  };
      return offer ;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }

};

module.exports=[schema,get];