var schema={
        divergentGrade:'string:min=1,max=1,required',
        dailyPayment:'number',
        sellRate:'number',
        term:'number',
        amountFunded:'number',
        amountPayback:'number',
        commission:'number',
        commissionPercentage:'number',
        applicationNumber:'string:required',
        durationType:'string',
        commisionFormula:'string',
        advanceFormula:'string',
        monthlyRevenue: 'number',
        averageDailyBalance: 'number',
        divergentReturn:'number',
        divergentFR:'number',
        afterDefaultAssumption:'number',
        averageLife:'number',
        iir:'number',
        durationIncremental:'number',
        sellIncremental:'number',
};

function* set({
    $get,
    $set,
    $api,
    value:{
        applicationNumber,
        divergentGrade,
        dailyPayment,
        durationType,
        sellRate,
        term,
        amountFunded,
        amountPayback,
        commission,
        commissionPercentage,
        divergentReturn,
        divergentFR,
        afterDefaultAssumption,
        averageLife,
        iir,
    },
    $debug,
    $configuration
}){
      const payLoad={
          	dealOffers : [{
			CommissionAmount: commission,
			CommissionRate: commissionPercentage,
            AmountFunded: amountFunded,
			LoanAmount: amountFunded,
			PaymentAmount: dailyPayment,
			RepaymentAmount: amountPayback,
			SellRate: sellRate,
			Term: term,
            TermPayment : dailyPayment, 
            LenderReturn : divergentReturn,
            NetFundingRate : divergentFR,        
            AfterDefaultAssumption : afterDefaultAssumption,
            AverageLife : averageLife,    
            IIR : iir,
	}
	]
      };
      const { 'application-processor': application } = yield $configuration('endpoints');
      const appResponse = yield $api.post(`${application}/application/${applicationNumber}/add/deal`,payLoad);
      return appResponse.body ;
};

module.exports=[schema,set];



 