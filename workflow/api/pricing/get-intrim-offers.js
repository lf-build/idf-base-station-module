var schema={
    applicationNumber:'string:min=2,max=100,required',
    divergentGrade:'string',
    amountFunded:'number',
    amountPayback:'number',
    commission:'number',
    commissionPercentage:'number',
    commisionFormula:"string",
    advanceFormula:"string",
    dailyPayment:'number',
    sellRate:'number',
    term:'number',
    divergentReturn:'number',
    divergentFR:'number',
    afterDefaultAssumption:'number',
    averageLife:'number',
    iir:'number',
    monthlyRevenue: 'number',
    averageDailyBalance: 'number',
    durationType:'string',
    durationIncremental:'number',
    sellIncremental:'number',
    range:{
                        dailyPayment:{
                            min:'number',
                            max:'number'
                        },
                        sellRate:{
                            min:'number',
                            max:'number'
                        },
                        term:{
                            min:'number',
                            max:'number'
                        }
                     }
};

function* get({
$get,
$set,
$api,
value:{divergentGrade,applicationNumber,amountFunded,amountPayback,
    commission,commissionPercentage,term,sellRate
,dailyPayment,divergentReturn,divergentFR,afterDefaultAssumption,durationType,
averageLife,iir,monthlyRevenue,averageDailyBalance,range,durationIncremental,sellIncremental},
$debug,
$configuration,
}){
  try{
      const payload={
                        amountFunded,
                        payBackAmount:amountPayback,
                        commission,
                        commissionRate:commissionPercentage,
                        terms:term,
                        rate:sellRate,
                        durationIncremental:durationIncremental,
                        sellIncremental:sellIncremental,
                        minDuration:range.term.min,
                        maxDuration:range.term.max,
                        minFactor:range.sellRate.min,
                        maxFactor:range.sellRate.max,
                        dailyPayment,
                        divergentReturn,
                        divergentFR,
                        afterDefaultAssumption,
                        averageLife,
                        iir
      };
     
      const { 'application-processor': application } = yield $configuration('endpoints');
      const response = yield $api.post(`${application}/application/${applicationNumber}/pricingcalculator`,payload);
      
      const offer={
                 applicationNumber:applicationNumber,
                 divergentGrade:divergentGrade,
                 amountFunded:response.body.intermediateData.amountFunded,
                 amountPayback:response.body.intermediateData.payBackAmount,
                 commission:response.body.intermediateData.commission,
                 commissionPercentage:response.body.intermediateData.commissionRate,
                 commisionFormula:"formula1",
                 advanceFormula:"formula2",
                 dailyPayment:response.body.intermediateData.dailyPayment,
                 sellRate:response.body.intermediateData.rate,
                 term:response.body.intermediateData.terms,
                 divergentReturn:response.body.intermediateData.divergentReturn,
                 divergentFR:response.body.intermediateData.divergentFR,
                 afterDefaultAssumption:response.body.intermediateData.afterDefaultAssumption,
                 averageLife:response.body.intermediateData.averageLife,
                 iir:response.body.intermediateData.iir,
                 durationIncremental:response.body.intermediateData.durationIncremental,
                 sellIncremental:response.body.intermediateData.sellIncremental,
                 monthlyRevenue: monthlyRevenue,
                 averageDailyBalance:averageDailyBalance,
                 durationType:durationType,
                 range:range
                  };

     return offer;
  } catch(e) {
      $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }

};

module.exports=[schema,get];