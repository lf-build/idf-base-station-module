var schema={
    applicationNumber:'string:min=2,max=100,required',
};

/**
 * 
 * Get generated deal
 * 
 * @category Application
 * @section API
 * @name get deal
 * 
 * @api public
 * 
 */

function* get({
$get,
$set,
$api,
value:{applicationNumber},
$debug,
$configuration,
}){
  try{
      const { 'offer-engine': offers } = yield $configuration('endpoints');
      const response = yield $api.get(`${offers}/application/${applicationNumber}/deal`);
      return response.body ;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }

};

module.exports=[schema,get];