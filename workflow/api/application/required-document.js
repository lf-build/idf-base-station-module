const schema={
      applicationNumber:'string:min=2,max=100,required'
};

/**
 * 
 * Get required documents
 * 
 * @category Application Document
 * @section API
 * @name get required document
 * 
 * @api public
 * 
 */

function* get({
    $get,
    $set,
    value:{applicationNumber},
    $api,
    $debug,
    $configuration,
}){
    const { 'verification-engine': verificationEngine } = yield $configuration('endpoints');
    try{
        const stipulationList = yield $api.get(`${verificationEngine}/application/${applicationNumber}/documents`);
        return stipulationList.body;
    }catch(e){
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }  
};

module.exports=[schema,get];