const schema = {
    referenceId: 'number',
    pageSize: 'number',
};

/**
 * 
 * Get application list
 * 
 * @category Application
 * @section API
 * @name get application list
 * 
 * @api public
 * 
 */

function * get({
    $api,
    $set,
    value: {
        referenceId, pageSize,
    },
    $debug,
    $configuration
}) {
    try{
        const { 'application-filters': applicationFilters } = yield $configuration('endpoints');
        const filter = {
            "searchFilterView": {},
            "sorts": {
                'ApplicationDate.Time.Tick' : 1
            }
        };

        if (referenceId) {
            filter.searchFilterView = {
                        'ApplicationDate.Time.Ticks' : {
                            "$gt": referenceId,
                        }
                    }
        }
    
        const response = yield $api.post(`${applicationFilters}/search-applications-free?page=${1}&pageSize=${pageSize || 100}`, filter);
        return {
            referenceId: response.body.length > 0 ? response.body[response.body.length - 1].ApplicationDate.Time.Ticks : referenceId,
            items: response.body,
            pageSize: response
                .headers
                .get('X-PAGE-SIZE'),
            totalCount: response
                .headers
                .get('X-TOTAL-COUNT'),
            totalPages: response
                .headers
                .get('X-TOTAL-PAGES')

        };
    }
    catch(e){
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports = [schema, get];