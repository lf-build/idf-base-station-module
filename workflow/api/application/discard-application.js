var schema={
    applicationNumber:'string:min=2,max=100,required',
    rejectCode:'string:min=1,max=10,required',
    reason:'string',
};

function* get({
    $get,
    $set,
    $api,
    value:{applicationNumber, rejectCode, reason},
    $debug,
    $configuration
}){

  /*
  rejectCode:
  700.40 = Declined
     "reasons": { }
  700.20 = Not interested
    "reasons": {
        Clientnotinterested
    }
  */
  try{
    const { 'status-management': statusManagement } = yield $configuration('endpoints');
    let finalReason = '';
    if(reason){
      finalReason = `/${reason}`;
    }
    const Response = yield $api.post(`${statusManagement}/application/${applicationNumber}/${rejectCode}${finalReason}`);
    return Response;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Transition not allowed',
      code: 400,
    }
  }
};

module.exports=[schema,get];