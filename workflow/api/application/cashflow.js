var schema={
    applicationNumber:'string:min=2,max=100,required',
};

/**
 * 
 * Get cashflow details
 * 
 * @category Application
 * @section API
 * @name get cashflow
 * 
 * @api public
 * 
 */

function* get({
    $get,
    $set,
    $api,
    value:{applicationNumber},
    $debug,
    $configuration
}){
    try{
          const { dataAttributes } = yield $configuration('endpoints');
          const response = yield $api.get(`${dataAttributes}/application/${applicationNumber}/CashflowVerificationData`);
          return response.body;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];