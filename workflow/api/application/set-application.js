const schema = {
    owners:{
      '@items':{
          firstName: 'string:min=2,max=100,required',
          lastName: 'string:min=2,max=100,required',
          ssn:'string:min=9,max=9,required',
          primaryPhone:'string:min=10,max=10,required',
          secondaryPhone:'string:min=10,max=10',
          emailAddress:'email:required',
          homeAddress:'string:min=2,max=100,required',
          city:'string:min=2,max=100,required',
          state:'string:min=2,max=100,required',
          zipCode:'string:min=5,max=5,required',
          dateOfBirth:'string:required',
      },
      min:1,
      max:4,
    },
    businessLegalName:'string:min=2,max=100,required',
    dba:'string:required',
    ein:'string:required',
    businessAddress:'string:min=2,max=100,required',
    city:'string:min=2,max=100,required',
    state:'string:min=2,max=100,required',
    zipCode:'string:min=5,max=5,required',
    officePhone:'string:min=10,max=10,required',
    requestedAmount:'number:positive,precision=2,required',
    consents:'boolean',
    partnerUserId:'string:required',
    applicationNumber: 'string',
};

/**
 * 
 * Create application
 * 
 * @category Application
 * @section API
 * @name create application
 * 
 * @api public
 * 
 */

function* set({
    $get,
    $set,
    $api,
    value, 
    $debug,  
    $configuration
}){
 try{
  const applicationSubmitPayload = value;
  const { 'application-processor': application } = yield $configuration('endpoints');
  const appResponse = yield $api.post(`${application}/application/submit/business`, applicationSubmitPayload);
  appResponse.body.draft=false;
  return appResponse.body ;
  } catch(e) {
    $debug(e);
    throw e;
  }
}

module.exports=[schema,set];