var schema={
    applicationNumber:'string:min=2,max=100,required',
};

function* get({
    $get,
    $set,
    $api,
    value:{applicationNumber},
    $debug,
    $configuration
}){
    try{
      if(applicationNumber=='new'){
        return { 
          owners: [{}],
          draft: false,
        }
       }else{
          const { 'application-processor': application } = yield $configuration('endpoints');
          const appResponse = yield $api.get(`${application}/application/${applicationNumber}`);
          appResponse.body.draft= false;
          delete appResponse.body.partnerId;
          return appResponse.body;
       }
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];