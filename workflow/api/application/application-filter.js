const schema={
    applicationNumber:'string:min=2,max=100,required',
};

/**
 * 
 * Get application details
 * 
 * @category Application
 * @section API
 * @name get application details
 * 
 * @api public
 * 
 */

function* get({
    $get,
    $set,
    $api,
    value:{applicationNumber},
    $debug,
    $configuration
}){
    try{
        const { 'application-filters': applicationFilters } = yield $configuration('endpoints');
        const filter = {
            "searchFilterView": {
                "ApplicationNumber": applicationNumber
            }
        };
        var response = yield $api.post(`${applicationFilters}/application/search-applications-free`, filter);
        return response.body[0];
    }
    catch(e){
        $debug(e);
        if(e.status.code === 404){
            throw{
                message: 'Application not found',
                code: 404,
            }
        }
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports=[schema,get];