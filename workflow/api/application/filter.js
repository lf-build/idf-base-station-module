const schema={
    applicationNumber:'string:min=2,max=100,required',
};

function* get({
    $get,
    $set,
    $api,
    value:{applicationNumber},
    $debug,
    $configuration
}){
    try{
        const { 'application-filters': applicationFilters } = yield $configuration('endpoints');
        const filter = {
            "searchFilterView": {
                "ApplicationNumber": applicationNumber
            }
        };
        return yield $api.post(`${applicationFilters}/search-applications-free`, filter);
    }
    catch(e){
        $debug(e);
        if(e.status.code === 404){
            throw{
                message: 'Application not found',
                code: 404,
            }
        }
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports=[schema,get];