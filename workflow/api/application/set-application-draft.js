const schema = {
     owners:{
      '@items':{
          firstName: 'string:min=2,max=100',
          lastName: 'string:min=2,max=100',
          ssn:'string:min=9,max=9',
          primaryPhone:'string:min=10,max=10',
          secondaryPhone:'string:min=10,max=10',
          emailAddress:'email:required',
          homeAddress:'string:min=2,max=100',
          city:'string:min=2,max=100',
          state:'string:min=2,max=100',
          zipCode:'string:min=5,max=5',
          dateOfBirth:'string',
      },
      min:1,
      max:4,
    },
    businessLegalName:'string:min=2,max=100',
    dba:'string',
    ein:'string',
    businessAddress:'string:min=2,max=100',
    city:'string:min=2,max=100',
    state:'string:min=2,max=100',
    zipCode:'string:min=5,max=5',
    officePhone:'string:min=10,max=10',
    requestedAmount:'number:positive,precision=2',
    consents:'boolean',
    partnerUserId:'string:required',
    applicationNumber: 'string',
};


/**
 * 
 * Create lead
 * 
 * @category Lead
 * @section API
 * @name create lead
 * 
 * @api public
 * 
 */

function* set({
    $get,
    $set,
    $api,
    value,  
    $debug,  
    $configuration
}){
 try{  
      const applicationSubmitPayload = value;
      const { 'application-processor': application } = yield $configuration('endpoints');
      const appResponse = yield $api.post(`${application}/application/lead`, applicationSubmitPayload);
      appResponse.body.draft=true;
      return appResponse.body ;
  } catch(e) {
    $debug(e);
    throw e;
  }
}

module.exports=[schema,set];