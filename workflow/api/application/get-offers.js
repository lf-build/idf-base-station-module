var schema={
    applicationNumber:'string:min=2,max=100,required',
};

/**
 * 
 * Get generated offers
 * 
 * @category Application
 * @section API
 * @name get offers
 * 
 * @api public
 * 
 */

function* get({
$get,
$set,
$api,
value:{applicationNumber},
$debug,
$configuration,
}){
  try{
      const { 'offer-engine': offers } = yield $configuration('endpoints');
      const response = yield $api.get(`${offers}/application/${applicationNumber}/offerengine/getapplicationoffers`);
      return response.body ;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }

};

module.exports=[schema,get];