const schema={
    applicationNumber:'string:min=2,max=100,required',
};

function* get({
    $get,
    $set,
    $api,
    value:{applicationNumber},
    $debug,
    $configuration,
    $stages: {
    'api': {
      'application': {
        'filter': applicationFilter
      }
    }
  },
}){
    try{
        const response = yield applicationFilter.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});
        const finalResponse = {};
        if(response.body && response.body.length > 0){
            finalResponse.applicantName = (response.body[0].Owners && response.body[0].Owners.length > 0) ? `${response.body[0].Owners[0].FirstName} ${response.body[0].Owners[0].LastName}` : '';
            finalResponse.legalBusinessName = response.body[0].LegalBusinessName;
            finalResponse.email = (response.body[0].Owners && response.body[0].Owners.length > 0) ? response.body[0].Owners[0].EmailAddress : '';
            finalResponse.phoneNumber = (response.body[0].Owners && response.body[0].Owners.length > 0) ? response.body[0].Owners[0].PhoneNumber : '';
            finalResponse.applicationNumber = response.body[0].ApplicationNumber;
            finalResponse.partnerId = response.body[0].PartnerId;
            finalResponse.status=response.body[0].StatusName;
            finalResponse.code=response.body[0].StatusCode;
            finalResponse.tags=response.body[0].tags
        }
        return finalResponse;
    }
    catch(e){
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports=[schema,get];