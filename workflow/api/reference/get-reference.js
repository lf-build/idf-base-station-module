var schema={
    applicationNumber:'string:min=2,max=100,required',
};

/**
 * 
 * Get reference
 * 
 * @category Reference
 * @section API
 * @name get
 * 
 * @api public
 * 
 */

function* get({
    $get,
    $set,
    $api,
    value:{applicationNumber},
    $debug,
    $configuration
}){
    try{
          const { application } = yield $configuration('endpoints');
          const response = yield $api.get(`${application}/${applicationNumber}/externalreferences`);
          return response.body;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];