var schema={
    applicationNumber:'string:min=2,max=100,required',
    references:{
      '@items':{
          name: 'string:min=2,max=100,required',
          value: 'string:min=2,max=100,required',
      },
      min:1,
    },
};

/**
 * 
 * Add reference
 * 
 * @category Reference
 * @section API
 * @name set
 * 
 * @api public
 * 
 */

function* set({
    $get,
    $set,
    $api,
    value:{applicationNumber,references},
    $debug,
    $configuration
}){
    try{
          const requestPayload = {
              ExternalReferences : references
          };
          const { application } = yield $configuration('endpoints');
          const response = yield $api.post(`${application}/${applicationNumber}/externalreferences`, requestPayload);
          return response.body;
  } catch(e) {
      $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,set];