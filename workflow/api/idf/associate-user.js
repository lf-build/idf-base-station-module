const schema = {
    userId: 'string:min=2,max=100,required',
    applicantId: 'string:min=2,max=100,required',
};

function *set({
  $api,
  $configuration,
  value:{ userId, applicantId}, 
  $debug
 })  
  {
  const { identity: identityEndpoint } = yield $configuration('endpoints');
  try{
    const response = yield $api.put(`${identityEndpoint}/${applicantId}/associate/user/${userId}`);
    return response.body;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Unable to associate user with applicant',
      code: 400,
    }
  }
};

module.exports = [schema, set];
