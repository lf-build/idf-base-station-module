const schema = {
    applicationNumber: 'string:min=2,max=100,required',
};

function *set({
  $api,
  $configuration,
  value:{ applicationNumber }, 
  $stages: {
    'api': {
        'application':{
        'application-filter':applicationFilter
    }
    },
    'authorization': {
        'identity':{
        'check':checkUser
     }
    },
    
},
  $debug
 })  
  {
  try{
    const filterResponse = yield applicationFilter.$execute
    .bind(undefined, {
        applicationNumber:applicationNumber,
    })({});
    const email = filterResponse.BusinessEmail;
    const businessApplicantName=filterResponse.BusinessApplicantName;
    const userResponse = yield checkUser.$execute
    .bind(undefined, {
        username:email,
    })({});
    userResponse.businessApplicantName=businessApplicantName;
    return userResponse;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Unable to associate user with applicant',
      code: 400,
    }
  }
};

module.exports = [schema, set];
