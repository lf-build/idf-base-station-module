var schema={
     applicationNumber:'string:min=2,max=100,required',
};

function* get({
    $get,
    $set,
    $api,
    value:{applicationNumber},
    $debug,
    $configuration,
    $stages: {
    'api': {
      'application': {
        'filter': applicationFilter
      }
    }
  },
}){
    let finalResponse = {};
    let bankLinkingStatus = true;
    let emailResponse={};
    try{
    const { 'email-service': emailservice } = yield $configuration('endpoints');
    const response = yield applicationFilter.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});
       
        if(response.body && response.body.length > 0){
            finalResponse.phoneNumber = (response.body[0].Owners && response.body[0].Owners.length > 0) ? response.body[0].Owners[0].PhoneNumber : '';
            if(response.body[0].tags && response.body[0].tags.length > 0){
                if(response.body[0].tags.filter((p) => p === 'Pending Credit Report Consent').length > 0 || response.body[0].tags.filter((p) => p === 'Pending Bank Linking').length > 0 ){
                bankLinkingStatus = false;
                }
            }
        }

        emailResponse=yield $api.get(`${emailservice}/application/${applicationNumber}/by/template/banklink`);
    } catch(e){
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
    //emailStatus is always true
    finalResponse.emailStatus = true;
    //Pending endpoint for Email html
    finalResponse.emailBody = emailResponse.body;
    finalResponse.bankLinkingStatus = bankLinkingStatus;
    //underWritingStatus is always false
    finalResponse.underWritingStatus = false;
    return finalResponse;
};

module.exports=[schema,get];