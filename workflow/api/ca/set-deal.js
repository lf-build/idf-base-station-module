var schema={
                     applicationNumber:'string:min=2,max=100,required',
                     LoanAmount:'number',
                     SellRate:'number',
                     BuyRate:'number',
                     Term:'number',
                     ApprovedAmount:'number',
                     TypeOfPayment:'string',
                     CommissionAmount:'number',
                     CommissionRate:'number',
                     PaymentAmount:'number',
                     NumberOfPayment:'number',
                     OriginatingFeeAmount:'number',
                     AchFee:'number',
                     RepaymentAmount:'number',
                     MinDuration:'number',
                     MaxDuration:'number',
                     MinFactor:'number',
                     MaxFactor:'number',
                     FixedOriginatingFee:'number',
                     OriginatingFeePer:'number',
                     OriginatingFeeMinLoanAmount:'number',
                     AmountFunded:'number',
                     ExpectedYield:'number',
                     DurationType:'string',
                     IsDealAdded:'boolean',
                     Ranges:{
                        LoanAmount:{
                            Min:'number',
                            Max:'number'
                        },
                        SellRate:{
                            Min:'number',
                            Max:'number'
                        },
                        Term:{
                            Min:'number',
                            Max:'number'
                        }
                     }
                 };
function* set({
    $get,
    $set,
    $api,
    value:{applicationNumber, LoanAmount,SellRate, Term,BuyRate,ApprovedAmount,TypeOfPayment,
           CommissionAmount, CommissionRate, PaymentAmount, NumberOfPayment,OriginatingFeeAmount,
           AchFee,RepaymentAmount, MinDuration, MaxDuration,MinFactor,MaxFactor,
           FixedOriginatingFee,OriginatingFeeMinLoanAmount, OriginatingFeePer
           ,AmountFunded, ExpectedYield,Ranges,DurationType},
    $debug,
    $configuration
}){
      const payLoad={
          	dealOffers : [{
            AmountFunded:AmountFunded,
            CommissionAmount:CommissionAmount,
            CommissionRate:CommissionRate,
            DurationType:DurationType,
            LoanAmount:LoanAmount,
            RepaymentAmount:RepaymentAmount,
            SellRate:SellRate,
            Term:Term,
            ApprovedAmount:ApprovedAmount,
            TypeOfPayment:"20",
            TypeOfPaymentText:"Daily",
            PaymentAmount:PaymentAmount,
            NumberOfPayment:NumberOfPayment,
            OriginatingFeeAmount:OriginatingFeeAmount,
            ACHFee:AchFee,
            BuyRate:BuyRate,
            ExpectedYield:ExpectedYield
        }
	]
      };
      const { 'application-processor': application } = yield $configuration('endpoints');
      const appResponse = yield $api.post(`${application}/application/${applicationNumber}/add/deal`,payLoad);
      return appResponse.body ;
};

module.exports=[schema,set];



 