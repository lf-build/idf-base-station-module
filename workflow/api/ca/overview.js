var schema={
    applicationNumber:'string:required',
};

/**
 * 
 * Get overview page details
 * 
 * @category Overview
 * @section API
 * @name get application overview
 * 
 * @api public
 * 
 */

function* get({
    $get,
    $set,
    $api,
    value:{applicationNumber},
    $debug,
    $configuration,
    $stages: {
    'api': {
      'ca': {
        'get-application': getApplication,
        'lookup': lookup
      }
    }
  },
}){
   try{

        //get application data
        const application = yield getApplication.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});
       
        //get lookup data
        const lookups = yield lookup.$execute
            .bind(undefined, {})({});

        application.purposeOfLoan = lookups.PurposeOfLoan[application.purposeOfLoan];
        application.loanTimeFrame = lookups.LoanTimeFrame[application.loanTimeFrame];
        application.industry = lookups.Industry[application.industry];
        application.businessType = lookups.BusinessTypes[application.businessType];
        application.propertyType = lookups.PropertyType[application.propertyType];
        application.addressType = lookups.PropertyType[application.addressType];

        return application;

  } catch(e) {
    $debug(e);
     throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];