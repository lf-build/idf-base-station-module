var schema={
    applicationNumber:'string:required'
};

/**
 * 
 * Get profile details
 * 
 * @category Profile
 * @section API
 * @name get profile details
 * 
 * @api public
 * 
 */

function* get({
   $get,
    $set,
    $api,
    value:{applicationNumber},
    $debug,
    $configuration,
    $stages: {
        'api': {
        'application':{
            'filter':applicationFilter
        }
        }
    }
}){
 try{
     //get application from applicantid
  
     const filterResponse = yield applicationFilter.$execute
        .bind(undefined, {
            applicationNumber:applicationNumber,
        })({});

  let profile={
      firstName:filterResponse.body[0].ContactFirstName,
      lastName:filterResponse.body[0].ContactLastName,
      dateOfBirth:"22/02/1992",
      mobilePhone:filterResponse.body[0].BusinessPhone,
      street:filterResponse.body[0].BusinessAddressLine1,
      city:filterResponse.body[0].BusinessCity,
      postalCode:filterResponse.body[0].BusinessZipCode,
      emailAddress:filterResponse.body[0].BusinessEmail
  };

  return profile;

  } catch(e) {
      $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
}

module.exports=[schema,get];