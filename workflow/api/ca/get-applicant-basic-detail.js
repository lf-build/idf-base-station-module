var schema={
    userId:'string:min=2,max=100,required',
};

function* get({
    $get,
    $set,
    $api,
    value:{userId},
    $debug,
    $configuration,
    $stages: {
        'api': {
        'ca': {
            'get-applicant-detail': getApplicant,
            'get-application-by-applicantid': getApplication,
            'lookup': lookup
        },
        'application':{
            'application-filter':applicationFilter
        }
        }
    }
}){
   try{
    const { 'applicant-detail' : applicationDetail, application } = yield $configuration('endpoints');

    let finalResponse = {};
    //get applicantid from userid
    const response = yield getApplicant.$execute
        .bind(undefined, {
            userId,
        })({});

    //get application from applicantid
    const applicationResponse = yield getApplication.$execute
        .bind(undefined, {
            applicantId: response[0].id,
        })({});

     const filterResponse = yield applicationFilter.$execute
        .bind(undefined, {
            applicationNumber:applicationResponse[0].applicationNumber,
        })({});

         //get purpose of loan from lookup
     const lookups = yield lookup.$execute
        .bind(undefined, {})({}); 
    
    finalResponse.userId = userId;
    finalResponse.applicantId = response[0].id;
    finalResponse.email = applicationResponse[0].primaryEmail.email;
    finalResponse.phoneNumber = applicationResponse[0].primaryPhone.phone;
    finalResponse.loanPurpose = lookups.PurposeOfLoan[applicationResponse[0].purposeOfLoan];
    finalResponse.loanAmount = applicationResponse[0].requestedAmount;
    finalResponse.applicationNumber = applicationResponse[0].applicationNumber;
    finalResponse.userName = `${applicationResponse[0].contactFirstName} ${applicationResponse[0].contactLastName}`;
    finalResponse.status=filterResponse.StatusName;
    finalResponse.code=filterResponse.StatusCode;
    finalResponse.source=lookups.SourceType[filterResponse.SourceType];

    return finalResponse;

  } catch(e) {
      $debug(e);
     throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];