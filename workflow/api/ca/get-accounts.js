var schema={
    applicationNumber:'string:min=2,max=100,required',
};

/**
 * 
 * Get Plaid linked bank details and Bank Statement documents
 * 
 * @category Bank Linking
 * @section API
 * @name get
 * 
 * @api public
 * 
 */

function* get({
    $get,
    $set,
    $api,
    value:{applicationNumber},
    $debug,
    $configuration
}){
   try{
    const { 'application-processor': application , 'verification-engine': verificationEngine, 'document-service': documentService } = yield $configuration('endpoints');
    
    let finalResponse = {};

    try{
      const accounts = yield $api.get(`${application}/application/${applicationNumber}/account/cashflow`);
      finalResponse.accounts = accounts.body;
    } catch(e){
        $debug(e);
        finalResponse.accounts = {};
    }

    let voidedCheckDocs = []; 
    try{
        const voidedCheckFiles = yield $api.get(`${documentService}/${applicationNumber}/bankstatement`);
        for(index in voidedCheckFiles.body){
                const voidedCheckDoc = {
                    fileName: voidedCheckFiles.body[index].fileName,
                    fileId: voidedCheckFiles.body[index].id,
                }
                voidedCheckDocs.push(voidedCheckDoc);
        }
    }catch(e){
        $debug(e);
    }
    finalResponse.voidedCheck = {
            "status":voidedCheckDocs.length > 0 ? true : false,
            "files":voidedCheckDocs,
    };

    return finalResponse;
  } catch(e) {
    $debug(e);
     throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];