var schema={
      firstName:'string:min=2,max=100,required',
      lastName:'string:min=2,max=100,required',
      dateOfBirth:'string:required',
      mobilePhone:'string:min=10,max=10,required',
      street:'string:min=2,max=100,required',
      city:'string:min=2,max=100,required',
      postalCode:'string:min=5,max=5,required',
      emailAddress:'email:required',
      applicantId:'string:required'
};

/**
 * 
 * Set profile details
 * 
 * @category Profile
 * @section API
 * @name set profile details
 * 
 * @api public
 * 
 */

function* get({
    $get,
    $set,
    $api,
    value, 
    $debug,  
    $configuration
}){
 try{
  
  let profile=value;

  return profile;

  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
}

module.exports=[schema,get];