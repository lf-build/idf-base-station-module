const schema = {
  Email:'string:min=2,max=100,required',
  RequestedAmount:'number:positive,precision=2,required',
  PurposeOfLoan:'string',
  LoanTimeFrame:'string',
  DateNeeded: 'string',
  Password:'string',
  LegalBusinessName:'string:min=2,max=100,required',
  AddressLine1:'string:min=2,max=100,required',
  City:'string:min=2,max=100,required',
  State:'string:min=2,max=100,required',
  ZipCode:'string:min=5,max=5,required',
  AddressType: 'string',
  BusinessTaxID: 'string:min=2,max=100,required',
  SICCode: 'string',
  BusinessStartDate:'string:required',
  BusinessType: 'string',
  ContactFirstName: 'string:min=2,max=100,required',
  ContactLastName: 'string:min=2,max=100,required',
  AnnualRevenue: 'number:positive,precision=2,required',
  AverageBankBalances: 'number:positive,precision=2,required',
  HaveExistingLoan: 'string',
  Source: 'string',
  Industry:'string',
  Phone: 'string:min=10,max=10,required',
  Owners: {
      '@items':{ 
                ownerId:'string',
                Addresses: {
                      '@items':{  AddressLine1: 'string:min=2,max=100,required',
                                  City: 'string:min=2,max=100,required',
                                  ZipCode: 'string:min=5,max=5,required',
                                  State: 'string:min=2,max=100,required'},
                      min:1
                },
                Designation: 'string:min=2,max=100',
                DOB: 'string:required',
                FirstName: 'string:min=2,max=100,required',
                LastName: 'string:min=2,max=100,required',
                OwnershipPercentage: 'string:min=1,max=100,required',
                OwnershipType: 'number',
                EmailAddresses: {
                       '@items':{ Email: 'string:min=2,max=100,required' },
                        min:1
                },
                PhoneNumbers: {
                       '@items':{ Phone: 'string:min=10,max=10,required'},
                        min:1
                },
                SSN: 'string:required' 
      },
      min:1,
      max:5,
    },
    PropertyType: 'number',
    Signature:'string',
    ApplicationNumber:'string',
    ApplicantId:'string',
    PartnerUserId:'string',
    ClientIpAddress:'string'
};

/**
 * 
 * Create application
 * 
 * @category Application
 * @section API
 * @name create application
 * 
 * @api public
 * 
 */

function* set({
    $get,
    $set,
    $api,
    value, 
    $debug,  
    $configuration
}){
 try{
  const { 'application-processor': application } = yield $configuration('endpoints');
  const appResponse = yield $api.post(`${application}/application/submit/business`, value);
  return appResponse.body ;
} catch(e) {
  $debug(e)
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
}

module.exports=[schema,set];

