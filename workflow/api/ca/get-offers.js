var schema={
    applicationNumber:'string:min=2,max=100,required',
};

function* get({
$get,
$set,
$api,
value:{applicationNumber},
$debug,
$configuration,
$stages: {
    'api': {
      'application': {
        'get-offers': getOffers
      }
    }
  },
}){
  try{
        const { configuration } = yield $configuration('endpoints');
        const response = yield getOffers.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});

        const config= yield $api.get(`${configuration}/business-offerengine`);

        var minLoanAmount = Number.POSITIVE_INFINITY;
        var maxLoanAmount = Number.NEGATIVE_INFINITY;
        var minSellRate = Number.POSITIVE_INFINITY;
        var maxSellRate = Number.NEGATIVE_INFINITY;
        var minTerm = Number.POSITIVE_INFINITY;
        var maxTerm = Number.NEGATIVE_INFINITY;
        if(response && response.offers && response.offers.length > 0){
           var tmpMin=0,tmpMax=0;
           for (var i=response.offers.length-1; i>=0; i--) {
               tmpMin = response.offers[i].minAmount;
               tmpMax = response.offers[i].maxAmount;
               if (tmpMin < minLoanAmount) minLoanAmount = tmpMin;
               if (tmpMax > maxLoanAmount) maxLoanAmount = tmpMax;
           }

           tmpMin=0,tmpMax=0;
           for (var i=response.offers.length-1; i>=0; i--) {
               tmpMin = response.offers[i].minFactor;
               tmpMax = response.offers[i].maxFactor;
               if (tmpMin < minSellRate) minSellRate = tmpMin;
               if (tmpMax > maxSellRate) maxSellRate = tmpMax;
           }

           tmpMin=0,tmpMax=0;
           for (var i=response.offers.length-1; i>=0; i--) {
               tmpMin = response.offers[i].minDuration;
               tmpMax = response.offers[i].maxDuration;
               if (tmpMin < minTerm) minTerm = tmpMin;
               if (tmpMax > maxTerm) maxTerm = tmpMax;
           }
        }

      var filteredOfferForMaxDuration = response.offers.filter((offer) => offer.maxDuration == maxTerm);
    
      var currentOfferForMaxDuration = filteredOfferForMaxDuration[0];

       const pricing={
             applicationNumber:applicationNumber,
             CommissionAmount:0,
             CommissionRate:0.00,
             DurationType:config.body.DurationType,
             LoanAmount: currentOfferForMaxDuration.maxAmount,
             AmountFunded: currentOfferForMaxDuration.maxAmount,
             ApprovedAmount: currentOfferForMaxDuration.maxAmount,
             MinDuration:minTerm,
             MaxDuration:maxTerm,
             MinFactor:currentOfferForMaxDuration.minFactor,
             MaxFactor:currentOfferForMaxDuration.maxFactor,
             Term:maxTerm,
             BuyRate:currentOfferForMaxDuration.minFactor,
             SellRate:currentOfferForMaxDuration.maxFactor,
             TypeOfPayment:"4",
             CommissionAmount:0.00,
             CommissionRate:0.00,
             PaymentAmount:0.00,
             NumberOfPayment:0,
             OriginatingFeeAmount:0.00,
             AchFee:config.body.ACHFee,
             RepaymentAmount:0.00,
             FixedOriginatingFee:config.body.FixedOriginatingFee,
             OriginatingFeePer:config.body.OriginatingFeePer,
             OriginatingFeeMinLoanAmount:config.body.OriginatingFeeMinLoanAmount,
             ExpectedYield:0.00,
             IsDealAdded : (response && response.dealOffer) ? true : false,
             Ranges:{
               Term:{
                    Min:minTerm,
                    Max:maxTerm
               },
               SellRate:{
                   Min:minSellRate,
                   Max:maxSellRate
               },
               LoanAmount:{
                     Min:minLoanAmount,
                     Max:maxLoanAmount
               }
             }
           };

           return pricing ;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }

};

module.exports=[schema,get];