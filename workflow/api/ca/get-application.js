var schema={
    applicationNumber:'string:required',
};

/**
 * 
 * Get application details
 * 
 * @category Application
 * @section API
 * @name get application details
 * 
 * @api public
 * 
 */

function* get({
    $get,
    $set,
    $api,
    value:{applicationNumber},
    $debug,
    $configuration
}){
   try{
       const { 'application-processor': application } = yield $configuration('endpoints');
       const appResponse = yield $api.get(`${application}/application/${applicationNumber}/details`);
       return appResponse.body ;
  } catch(e) {
      $debug(e);
     throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];