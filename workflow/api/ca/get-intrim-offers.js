var schema={
                     applicationNumber:'string:min=2,max=100,required',
                     LoanAmount:'number',
                     SellRate:'number',
                     BuyRate:'number',
                     Term:'number',
                     ApprovedAmount:'number',
                     TypeOfPayment:'string',
                     CommissionAmount:'number',
                     CommissionRate:'number',
                     PaymentAmount:'number',
                     NumberOfPayment:'number',
                     OriginatingFeeAmount:'number',
                     AchFee:'number',
                     RepaymentAmount:'number',
                     MinDuration:'number',
                     MaxDuration:'number',
                     MinFactor:'number',
                     MaxFactor:'number',
                     FixedOriginatingFee:'number',
                     OriginatingFeePer:'number',
                     OriginatingFeeMinLoanAmount:'number',
                     AmountFunded:'number',
                     ExpectedYield:'number',
                     DurationType:'string',
                     IsDealAdded:'boolean',
                     Ranges:{
                        LoanAmount:{
                            Min:'number',
                            Max:'number'
                        },
                        SellRate:{
                            Min:'number',
                            Max:'number'
                        },
                        Term:{
                            Min:'number',
                            Max:'number'
                        }
                     }
                 };

function* get({
$get,
$set,
$api,
value:{applicationNumber, LoanAmount,SellRate, Term,BuyRate,ApprovedAmount,TypeOfPayment,
CommissionAmount, CommissionRate, PaymentAmount, NumberOfPayment,OriginatingFeeAmount,
AchFee,RepaymentAmount, MinDuration, MaxDuration,MinFactor,MaxFactor,
FixedOriginatingFee,OriginatingFeeMinLoanAmount, OriginatingFeePer
,AmountFunded, ExpectedYield,Ranges},
$debug,
$configuration,
}){
  try{
      const payload={
                     loanAmount:LoanAmount,
                     sellRate:SellRate,
                     buyRate:BuyRate,
                     term:Term                     ,
                     approvedAmount:ApprovedAmount,
                     typeOfPayment:TypeOfPayment,
                     commissionAmount:CommissionAmount,
                     commissionRate:CommissionRate,
                     paymentAmount:PaymentAmount,
                     numberOfPayment:NumberOfPayment,
                     originatingFeeAmount:OriginatingFeeAmount,
                     achFee:AchFee,
                     repaymentAmount:RepaymentAmount,
                     minDuration:MinDuration,
                     maxDuration:MaxDuration,
                     minFactor:MinFactor,
                     maxFactor:MaxFactor,
                     fixedOriginatingFee:FixedOriginatingFee,
                     originatingFeePer:OriginatingFeePer,
                     originatingFeeMinLoanAmount:OriginatingFeeMinLoanAmount,
                     amountFunded:AmountFunded,
                     expectedYield:ExpectedYield
      };
     
      const { 'application-processor': application } = yield $configuration('endpoints');
      const response = yield $api.post(`${application}/application/${applicationNumber}/pricingcalculator`,payload);
      const intrimData=response.body.intermediateData;
      const offer={
          applicationNumber:applicationNumber,
          LoanAmount:LoanAmount,
          SellRate:SellRate,
          BuyRate:BuyRate,
          Term:Term,
          ApprovedAmount:ApprovedAmount,
          TypeOfPayment:TypeOfPayment,
          CommissionAmount:intrimData._computeGMCommision,
          CommissionRate:intrimData.__computeFundingPoints,
          PaymentAmount:intrimData._calculatePaymentAmount,
          NumberOfPayment:intrimData._NumberOfPayment===null ? 0 : intrimData._NumberOfPayment,
          OriginatingFeeAmount:intrimData._countOriginatingfees,
          AchFee:AchFee,
          RepaymentAmount:intrimData._countTotalPayback,
          MinDuration:MinDuration,
          MaxDuration:MaxDuration,
          MinFactor:MinFactor,
          MaxFactor:MaxFactor,
          FixedOriginatingFee:FixedOriginatingFee,
          OriginatingFeePer:OriginatingFeePer,
          OriginatingFeeMinLoanAmount:OriginatingFeeMinLoanAmount,
          AmountFunded:intrimData._computeFundedAmount,
          ExpectedYield:intrimData._computeExpectedYield,
          Ranges:Ranges
            }
     return offer;
  } catch(e) {
      $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }

};

module.exports=[schema,get];


