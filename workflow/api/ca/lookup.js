var schema={};

/**
 * 
 * Get application lookup
 * 
 * @category Application
 * @section API
 * @name get lookup
 * 
 * @api public
 * 
 */

function* get({
    $get,
    $set,
    $api,
    value, 
    $debug,  
    $configuration
}){
 try{
  const { configuration, lookupservice: lookup } = yield $configuration('endpoints');

  var { body: lookupConfig } = yield $api.get(`${configuration}/application-lookup`);

  return Object.keys(lookupConfig)
    .reduce((key, value) => {
      return key.then((d) => {
        return $api.get(`${lookup}/${lookupConfig[value]}`)
            .then(({ body }) => {
              return Object.assign(d, { [value]: body } );
            }); 
      });
    }, Promise.resolve({}));

  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
}

module.exports=[schema,get];