const schema = {
  Email:'string:min=2,max=100,required',
  RequestedAmount:'number:required',
  PurposeOfLoan:'string',
  LoanTimeFrame:'string',
  DateNeeded: 'string',
  Password:'string',
  LegalBusinessName:'string:min=2,max=100',
  AddressLine1:'string:min=2,max=100',
  City:'string:min=2,max=100',
  State:'string:min=2,max=100',
  ZipCode:'string:min=5,max=5',
  AddressType: 'string',
  BusinessTaxID: 'string:min=2,max=100',
  SICCode: 'string',
  BusinessStartDate:'string',
  BusinessType: 'string',
  ContactFirstName: 'string:min=2,max=100',
  ContactLastName: 'string:min=2,max=100',
  AnnualRevenue: 'number:positive,precision=2',
  AverageBankBalances: 'number:positive,precision=2',
  HaveExistingLoan: 'string',
  Source: 'string',
  Industry:'string',
  Phone: 'string:min=10,max=10',
  Owners: {
      '@items':{ 
                ownerId:'string',
                Addresses: {
                      '@items':{  AddressLine1: 'string:min=2,max=100',
                                  City: 'string:min=2,max=100',
                                  ZipCode: 'string:min=5,max=5',
                                  State: 'string:min=2,max=100'
                               },
                },
                Designation: 'string:min=2,max=100',
                DOB: 'string',
                FirstName: 'string:min=2,max=100',
                LastName: 'string:min=2,max=100',
                OwnershipPercentage: 'string:min=1,max=100',
                OwnershipType: 'number',
                EmailAddresses: {
                       '@items':{ Email: 'string:min=2,max=100' },
                },
                PhoneNumbers: {
                       '@items':{ Phone: 'string:min=10,max=10'},
                },
                SSN: 'string' 
      },
      max:5,
    },
    PropertyType: 'number',
    Signature:'string',
    ApplicationNumber:'string',
    ApplicantId:'string',
    PartnerUserId:'string',
    ClientIpAddress:'string'
};

/**
 * 
 * Create lead
 * 
 * @category Lead
 * @section API
 * @name create lead
 * 
 * @api public
 * 
 */

function* set({
    $get,
    $set,
    $api,
    value, 
    $debug,  
    $configuration
}){
 try{
  const { 'application-processor': application } = yield $configuration('endpoints');
  const appResponse = yield $api.post(`${application}/application/lead`, value);
  return appResponse.body ;
  } catch(e) {
    $debug(e);
    if(e && e.body && e.body.code && e.body.code === 409){
      throw {
        message: 'Email Address already exist.',
        code: e.body.code,
      }
    }
    throw {
      message: e && e.body && e.body.message ? e.body.message : 'Something went wrong',
      code: 400,
    }
  }
}

module.exports=[schema,set];