const schema={
    applicationNumber:'string:min=2,max=100,required',
};

function* get({
    $get,
    $set,
    $api,
    value:{applicationNumber},
    $debug,
    $configuration,
    $stages: {
    'api': {
      'application': {
        'filter': applicationFilter
      }
    }
  },
}){
    try{
        const finalResponse = {};
        const response = yield applicationFilter.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});
        if(response.body && response.body.length > 0){

            let finalOwnerList = [];
            if(response.body[0].Owners && response.body[0].Owners.length > 0){
                response.body[0].Owners.forEach(function(element) {
                    finalOwnerList.push(`${element.FirstName} ${element.LastName}`);
                }, this);
            }

            finalResponse.applicants = finalOwnerList;
            finalResponse.loanAmount = response.body[0].AmountFunded;
            finalResponse.businessName = response.body[0].LegalBusinessName;
            finalResponse.repaymentAmount = response.body[0].RepaymentAmount;
            finalResponse.sellRate = response.body[0].SellRate;
            finalResponse.term = response.body[0].Term;
            finalResponse.commission = response.body[0].CommissionAmount;
        }
        return finalResponse;
    }
    catch(e){
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports=[schema,get];