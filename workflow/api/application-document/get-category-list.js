const schema={
};

/**
 * 
 * Get category list
 * 
 * @category Application Document
 * @section API
 * @name get document category
 * 
 * @api public
 * 
 */

function* get({
    $get,
    $set,
    $api,
    $debug,
    $configuration,
}){
    const {configuration } = yield $configuration('endpoints');
    try{
        let finalResponse = {};
        finalResponse = yield $api.get(`${configuration}/application-documents`);
        return finalResponse.body;
    }catch(e){
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports=[schema,get];