const schema={
      applicationNumber:'string:min=2,max=100,required',
      fileId:'string:min=2,max=100,required',
};

function* get({
    $get,
    $set,
    value:{applicationNumber, fileId},
    $api,
    $debug,
    $configuration,
}){
    const {'document-service': documentService } = yield $configuration('endpoints');
    try{
        const fileResponse = yield $api.get(`${documentService}/application/${applicationNumber}/${fileId}/downloaddocument`);
        return fileResponse.body.downloadString;
    }catch(e){
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports=[schema,get];