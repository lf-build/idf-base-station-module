const schema={
      applicationNumber:'string:min=2,max=100,required'
};

/**
 * 
 * Get docuSign status
 * 
 * @category DocuSign
 * @section API
 * @name get status
 * 
 * @api public
 * 
 */

function* get({
    $get,
    $set,
    value:{applicationNumber},
    $api,
    $debug,
    $configuration,
    $stages: {
    'api': {
      'application': {
        'filter': applicationFilter,
        'required-document': stipulationDoc,
      }
    }
  },
}){
    try{
       let finalResponse = {};
       let docuSignStatus = 'Signed';
       let docuSignVisible = false;
       const response = yield applicationFilter.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});
        if(response.body && response.body.length > 0){
           if(response.body[0].tags && response.body[0].tags.length > 0){
                if(response.body[0].tags.filter((p) => p === 'Pending Signature').length > 0){
                    docuSignStatus = 'Unsigned';
                }
            }
           if(response.body[0].StatusName === 'Agreement Docs Out' || response.body[0].StatusName === 'Funding Review'){
               docuSignVisible = true;
           }
        }
        finalResponse.docuSignStatus = docuSignStatus;
        finalResponse.docuSignVisible = docuSignVisible;
        return finalResponse;
    }
    catch(e){
        $debug(e);
         throw {
      message: 'Something went wrong',
      code: 400,
    }
    }
};

module.exports=[schema,get];