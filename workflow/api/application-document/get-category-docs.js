const schema={
      applicationNumber:'string:min=2,max=100,required',
      category:'string:min=2,max=100,required',
};

/**
 * 
 * Get category wise documents
 * 
 * @category Application Document
 * @section API
 * @name get category wise documents
 * 
 * @api public
 * 
 */

function* get({
    $get,
    $set,
    value:{applicationNumber, category},
    $api,
    $debug,
    $configuration,
}){
    const {'document-service': documentService } = yield $configuration('endpoints');
    try{
        let finalFileList = [];
        const categoryFileList = yield $api.get(`${documentService}/${applicationNumber}/${category}`);
        for(indx in categoryFileList.body) {
                        const file = categoryFileList.body[indx];
                        const fileResponse = yield $api.get(`${documentService}/application/${applicationNumber}/${file.id}/downloaddocument`);
                        finalFileList.push(Object.assign(file, fileResponse.body));
        }
        return finalFileList;
    }catch(e){
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports=[schema,get];