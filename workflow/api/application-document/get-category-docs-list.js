const schema={
      applicationNumber:'string:min=2,max=100,required',
      category:'string:min=2,max=100,required',
};

function* get({
    $get,
    $set,
    value:{applicationNumber, category},
    $api,
    $debug,
    $configuration,
}){
    const {'document-service': documentService } = yield $configuration('endpoints');
    try{
        let finalFileList = [];
        try{
            const categoryFileList = yield $api.get(`${documentService}/${applicationNumber}/${category}`);
            for(indx in categoryFileList.body) {
                finalFileList.push(categoryFileList.body[indx]);
            }
        } catch(e){
            $debug(e);
        }
        return finalFileList;
    }catch(e){
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
};

module.exports=[schema,get];