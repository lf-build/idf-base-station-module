const schema={
      applicationNumber:'string:min=2,max=100,required'
};


/**
 * 
 * Get all category documents
 * 
 * @category Application Document
 * @section API
 * @name get all documents
 * 
 * @api public
 * 
 */

function* get({
    $get,
    $set,
    value:{applicationNumber},
    $api,
    $debug,
    $configuration,
}){
    const {'document-service': documentService, configuration } = yield $configuration('endpoints');
    let finalResponse = [];
    try{
        let category = [];
        //Fetch list of category
        const categoryResponse = yield $api.get(`${configuration}/application-documents`);
        
        const borrowerdocumentCategory = categoryResponse.body.entities[0].ApplicationDocument.filter(function (obj) {
                        return obj.groupName === 'borrowerdocument';
        });
        if(borrowerdocumentCategory && borrowerdocumentCategory.length > 0){
            borrowerdocumentCategory[0].category.forEach(function(element) {
                category.push(element);
            });
        }
         const consentAndAgreementCategory = categoryResponse.body.entities[0].ApplicationDocument.filter(function (obj) {
                        return obj.groupName === 'consentandagreement';
        });
        if(consentAndAgreementCategory && consentAndAgreementCategory.length > 0){
            consentAndAgreementCategory[0].category.forEach(function(element) {
                category.push(element);
            });
        }
         const signeddocumentCategory = categoryResponse.body.entities[0].ApplicationDocument.filter(function (obj) {
                        return obj.groupName === 'signeddocument';
        });
        if(signeddocumentCategory && signeddocumentCategory.length > 0){
            signeddocumentCategory[0].category.forEach(function(element) {
                category.push(element);
            });
        }
        if(category && category.length > 0){
                for(index in category) {
                let catName = category[index];
                let finalFileList = [];
                const categoryFileList = yield $api.get(`${documentService}/${applicationNumber}/${catName}`);
                if(categoryFileList.body.length > 0){
                    //finalResponse.push({category: catName, files: categoryFileList.body});
                    for(indx in categoryFileList.body) {
                        const file = categoryFileList.body[indx];
                        const fileResponse = yield $api.get(`${documentService}/application/${applicationNumber}/${file.id}/downloaddocument`);
                        finalFileList.push(Object.assign(file, fileResponse.body));
                    }
                    finalResponse.push({category: catName, files: finalFileList});
                }
            }

        }
    }catch(e){
        $debug(e);
        throw {
            message: 'Something went wrong',
            code: 400,
        }
    }
    return finalResponse;
};

module.exports=[schema,get];