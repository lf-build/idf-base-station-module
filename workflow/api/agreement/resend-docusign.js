const schema = {
    applicationNumber: 'string:min=2,max=100,required',
};


/**
 * 
 * Resend docuSign mail
 * 
 * @category DocuSign
 * @section API
 * @name resend docuSign mail
 * 
 * @api public
 * 
 */

function* set({
    $get,
    $set,
    $api,
    value:{applicationNumber}, 
    $debug,  
    $configuration
}){
 try{
  const { 'application-processor': applicationProcessor } = yield $configuration('endpoints');
  const response = yield $api.post(`${applicationProcessor}/application/${applicationNumber}/agreement/resend`);
  return response.body ;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
}

module.exports=[schema,set];