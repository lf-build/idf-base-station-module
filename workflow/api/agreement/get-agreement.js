const schema={
      applicationNumber:'string:min=2,max=100,required'
};

function* get({
    $get,
    $set,
    value:{applicationNumber},
    $api,
    $debug,
    $configuration,
    $stages: {
    'api': {
      'application': {
        'filter': applicationFilter,
        'required-document': stipulationDoc,
      }
    }
  },
}){
    const {'verification-engine': verificationEngine } = yield $configuration('endpoints');

    //Fetch list of addition stipulation to show
    let taxReturnVerification = false;
    let confessionVerification = false;
    let lienVerification = false;
    try{
        const stipulationList = yield stipulationDoc.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});
        if(stipulationList.length > 0){
            stipulationList.forEach(function(element) {
                if(element.factName === 'TaxReturnVerification'){
                   taxReturnVerification = element.isRequested;
                }
                else if(element.factName === 'ConfessionVerification'){
                   confessionVerification = element.isRequested;
                }
                else if(element.factName === 'LienVerification'){
                   lienVerification = element.isRequested;
                }
            }, this);
        } 
    }catch(e){
        $debug(e);
    }


    //fetch Voided check documents
    let voidedCheckDocs = []; 
    try{
        const voidedCheckFiles = yield $api.get(`${verificationEngine}/application/${applicationNumber}/BankVerification/documentsDetails`);
        for(index in voidedCheckFiles.body){
            if(voidedCheckFiles.body[index].documentStatus === 'Verifiable'){
                const voidedCheckDoc = {
                    fileName: voidedCheckFiles.body[index].fileName,
                    fileId: voidedCheckFiles.body[index].id,
                }
                voidedCheckDocs.push(voidedCheckDoc);
            }
        }
    }catch(e){
        $debug(e);
    }

    //fetch PhotoId documents
    let photoIdDocs = []; 
    try{
        const photoIdFiles = yield $api.get(`${verificationEngine}/application/${applicationNumber}/IDVerification/documentsDetails`);
        for(index in photoIdFiles.body){
            if(photoIdFiles.body[index].documentStatus === 'Verifiable'){
                const photoIdDoc = {
                    fileName: photoIdFiles.body[index].fileName,
                    fileId: photoIdFiles.body[index].id,
                }
                photoIdDocs.push(photoIdDoc);
            }
        }
    }catch(e){
        $debug(e);
    }

    //fetch Contract documents
    let contractDocs = []; 
    try{
    const contractFiles = yield $api.get(`${verificationEngine}/application/${applicationNumber}/SignedDocumentVerification/documentsDetails`);
    for(index in contractFiles.body){
        if(contractFiles.body[index].documentStatus === 'Verifiable'){
                const contractDoc = {
                    fileName: contractFiles.body[index].fileName,
                    fileId: contractFiles.body[index].id,
                }
                contractDocs.push(contractDoc);
            }
        }
    }catch(e){
        $debug(e);
    }

    //fetch Tax return documents
    let taxReturnVerificationDocs = []; 
    try{
        const taxReturnFiles = yield $api.get(`${verificationEngine}/application/${applicationNumber}/TaxReturnVerification/documentsDetails`);
        for(index in taxReturnFiles.body){
            if(taxReturnFiles.body[index].documentStatus === 'Verifiable'){
                    const taxReturnVerificationDoc = {
                        fileName: taxReturnFiles.body[index].fileName,
                        fileId: taxReturnFiles.body[index].id,
                    }
                    taxReturnVerificationDocs.push(taxReturnVerificationDoc);
                }
            }
        if(taxReturnVerificationDocs.length > 0){
            taxReturnVerification = true;
        }
    }catch(e){
       $debug(e);
    }

    //fetch lien documents
    let lienVerificationDocs = []; 
    try{
        const lienVerificationFiles = yield $api.get(`${verificationEngine}/application/${applicationNumber}/LienVerification/documentsDetails`);
        for(index in lienVerificationFiles.body){
            if(lienVerificationFiles.body[index].documentStatus === 'Verifiable'){
                    const lienVerificationDoc = {
                        fileName: lienVerificationFiles.body[index].fileName,
                        fileId: lienVerificationFiles.body[index].id,
                    }
                    lienVerificationDocs.push(lienVerificationDoc);
                }
            }
        if(lienVerificationDocs.length > 0){
            lienVerification = true;
        }
    }catch(e){
        $debug(e);
    }

     //fetch confession documents
    let confessionVerificationDocs = []; 
    try{
        const confessionVerificationFiles = yield $api.get(`${verificationEngine}/application/${applicationNumber}/ConfessionVerification/documentsDetails`);
        for(index in confessionVerificationFiles.body){
            if(confessionVerificationFiles.body[index].documentStatus === 'Verifiable'){
                    const confessionVerificationDoc = {
                        fileName: confessionVerificationFiles.body[index].fileName,
                        fileId: confessionVerificationFiles.body[index].id,
                    }
                    confessionVerificationDocs.push(confessionVerificationDoc);
                }
            }
        if(confessionVerificationDocs.length > 0){
            confessionVerification = true;
        }
    }catch(e){
        $debug(e);
    }

    let email = '';
    let docuSignStatus = 'Signed';
    let finalizationStatus = true;
    try{
       const response = yield applicationFilter.$execute
        .bind(undefined, {
            applicationNumber: applicationNumber,
        })({});
        if(response.body && response.body.length > 0){
            email = (response.body[0].Owners && response.body[0].Owners.length > 0) ? response.body[0].Owners[0].EmailAddress : '';
            if(response.body[0].tags && response.body[0].tags.length > 0){
                if(response.body[0].tags.filter((p) => p === 'Pending Signature').length > 0){
                    docuSignStatus = 'Unsigned';
                }
                if(response.body[0].tags.filter((p) => p === 'Pending Signature').length > 0 || response.body[0].tags.filter((p) => p === 'Pending Documents').length > 0){
                    finalizationStatus = false;
                }
            }
        }
    }
    catch(e){
         $debug(e);
    }
    return {
        "voidedCheck":{
            "status":voidedCheckDocs.length > 0 ? true : false,
            "files":voidedCheckDocs,
        },
        "photoId":{
            "status":photoIdDocs.length > 0 ? true : false,
            "files":photoIdDocs,
        },
        "contract":{
            "status": (contractDocs.length > 0 || docuSignStatus === 'Signed' ) ? true : false,
            "files":contractDocs,
            docuSignStatus,
            "email": email,
        },
        "taxReturn":{
            "status":taxReturnVerificationDocs.length > 0 ? true : false,
            "files":taxReturnVerificationDocs,
            "isDisplay":taxReturnVerification,
        },
        "lien":{
            "status":lienVerificationDocs.length > 0 ? true : false,
            "files":lienVerificationDocs,
            "isDisplay":lienVerification,
        },
        "confession":{
            "status":confessionVerificationDocs.length > 0 ? true : false,
            "files":confessionVerificationDocs,
            "isDisplay":confessionVerification,
        },
        finalizationStatus,
    };
};

module.exports=[schema,get];