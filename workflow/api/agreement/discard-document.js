var schema = {
  applicationNumber:'string:min=2,max=100,required',
  fileId:'string:min=2,max=200,required',
  fileType:'string:min=2,max=100,required',
};

/**
 * 
 * Discard document
 * 
 * @category Application Document
 * @section API
 * @name discard document
 * 
 * @api public
 * 
 */

function *set({
    $get,
    $set,
    $api,
    value:{applicationNumber, fileId, fileType},
    $debug,
    $configuration
})  
{
  const { 'verification-engine': verificationEngine } = yield $configuration('endpoints');
 
  let documentType = "";
  switch (fileType)
  {
      case 'voidedcheck':
        documentType = "BankVerification";
        break;
      case 'photoid':
        documentType = "IDVerification";
        break;
      case 'signeddocument':
        documentType = "SignedDocumentVerification";
        break;
      case 'taxreturnstatement':
        documentType = "TaxReturnVerification";
        break;
      case 'confessionofjudgment':
        documentType = "ConfessionVerification";
        break;
      case 'liendocument':
        documentType = "LienVerification";
        break;
      default:
        break;
  }

  //Discard file
  try{
    yield $api.put(`${verificationEngine}/application/${applicationNumber}/${documentType}/${fileId}/Unverifiable/irrelevant`);
  }
  catch(e){
    $debug(e);
    throw {      
        message: 'Something went wrong',
        code: 400,
      }
  }
};

module.exports = [schema, set];