var schema = {
  applicationNumber:'string:min=2,max=100,required',
  fileId:'string:min=2,max=200,required',
};

/**
 * 
 * Get document based on fileId
 * 
 * @category Application Document
 * @section API
 * @name get document
 * 
 * @api public
 * 
 */

function *get({
    $get,
    $set,
    $api,
    value:{applicationNumber,fileId,fileName},
    $debug,
    $configuration
})  
{
  const { 'doc-download-service': downloadEngine, 'document-service': documentEngine } = yield $configuration('endpoints');
 
  //Fetch file
  try{
    let fileResponse = {};

    // For blob response
    // fileResponse = yield $api.get(`${downloadEngine}/application/${applicationNumber}/${fileId}/download`);

    // For base 64
    fileResponse = yield $api.get(`${documentEngine}/application/${applicationNumber}/${fileId}/downloaddocument`);
    return fileResponse.body;
  }
  catch(e){
    $debug(e);
    throw {      
        message: 'Something went wrong',
        code: 400,
      }
  }
};

module.exports = [schema, get];
