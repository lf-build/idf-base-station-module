const schema = {
    description:'string:min=2,max=100,required',
    applicationNumber:'string:min=2,max=100,required',
    title:'string:min=2,max=100,required',
};

/**
 * 
 * Add activity
 * 
 * @category Activity
 * @section API
 * @name add
 * 
 * @api public
 * 
 */


function* set({
    $get,
    $set,
    $api,
    value:{description,applicationNumber,title}, 
    $debug,  
    $configuration
}){
 try{
  const { eventhub } = yield $configuration('endpoints');
  const payload = { Description: description,
      ApplicationNumber: applicationNumber,
      Title: title
   };
  const response = yield $api.post(`${eventhub}/ApplicationActivityLogged`, payload);
  return response.body ;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
}

module.exports=[schema,set];