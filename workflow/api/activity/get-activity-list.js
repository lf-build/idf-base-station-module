var schema={
    applicationNumber:'string:min=2,max=100,required',
};

/**
 * 
 * Get activity title list
 * 
 * @category Activity
 * @section API
 * @name get activity title list
 * 
 * @api public
 * 
 */

function* get({
    $get,
    $set,
    $api,
    value:{applicationNumber},
    $debug,
    $configuration
}){
    try{
          const { 'status-management': statusManagement } = yield $configuration('endpoints');
          const response = yield $api.get(`${statusManagement}/application/${applicationNumber}`);
          return response.body.activities;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];