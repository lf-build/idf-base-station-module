var schema={
    applicationNumber:'string:min=2,max=100,required',
};

/**
 * 
 * Get application activity
 * 
 * @category Activity
 * @section API
 * @name get
 * 
 * @api public
 * 
 */

function* get({
    $get,
    $set,
    $api,
    value:{applicationNumber},
    $debug,
    $configuration
}){
    try{
          const { activityList } = yield $configuration('endpoints');
          const response = yield $api.get(`${activityList}/application/${applicationNumber}`);
          return response.body;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Something went wrong',
      code: 400,
    }
  }
};

module.exports=[schema,get];