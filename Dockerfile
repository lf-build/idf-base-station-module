FROM node:alpine

ARG NPM_TOKEN 
WORKDIR /app
ADD /package.json /app/package.json
RUN printf "//`node -p \"require('url').parse(process.env.NPM_REGISTRY_URL || 'https://registry.npmjs.org').host\"`/:_authToken=${NPM_TOKEN}\nregistry=${NPM_REGISTRY_URL:-https://registry.npmjs.org}\n" >> ~/.npmrc
RUN npm install --only=production
RUN rm -f .npmrc
RUN npm install -g pm2

ADD . /app

ENTRYPOINT pm2 start --no-daemon npm start -i 1
